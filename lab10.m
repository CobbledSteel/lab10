%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assignment:  lab10
% Lab Section: Wednesday, 3:30pm-5:30pm SC189
% Description: the program opens a file and conducts the following algorithm:
%              For each two adjacent numbers in the file, if the rightmost 
%              number is greater than the leftmost number, it adds all the 
%              values between the numbers, inclusive. It then prints the 
%              answer to the algorithm.
% Programmers: Ziheng Wang wang2194@purdue.edu
%              Vadim Nikiforov vnikifor@purdue.edu
%              Haozheng Qu qu34@purdue.edu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% the initializations below are not necessary, but are used for documentation purposes
userFile = fopen('testdata', 'r'); % open the external file
answer = 0; % initialize the answer to the algorithm
num1 = 0; % initialize the first number of each comparison
num2 = 0; % initialize the second number of each comparison
temp = 0; % initialize a temporary variable for use in a loop

% updating the num1 at the beginning
num2 = fscanf(userFile, '%d', 1);  % get data outside of the loop to avoid comparing the first number to 0
% select each adjacent pair of numbers throughout the data range
while ~feof(userFile) 
  % get a new pair of adjacent numbers
  num1 = num2;
  num2 = fscanf(userFile, '%d', 1);
  % ensure that there are no EOF values in the comparison
  if ~feof(userFile)
    % only add the numbers if the first number is smaller
    if num1 < num2
      temp = 0;
      % add all numbers from the smaller to the larger number, inclusive
      for i = num1 : num2
        temp += i;
      end
      answer += temp;
    end
  end
end

fprintf('Final value: %d\n', answer); % output final answer
fclose(userFile); % close the external file
